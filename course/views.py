from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse
from django.http.response import HttpResponse
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from course.forms import StudentForm
from django.views.generic.list import ListView
from django.core import serializers


from course.models import Student, Subject


@login_required
def home(request):
    user = request.user
    if user.has_perm('course.is_teacher'):
        return redirect(reverse('home_teacher'))
    elif user.has_perm('course.is_student'):
        return redirect(reverse('home_student'))
    else:
        return render(request, template_name='home.html')


@permission_required('course.is_teacher')
def home_teacher(request):
    return render(request, template_name='home_teacher.html')


@permission_required('course.is_student')
def home_student(request):
    return render(request, template_name='home_student.html')


def list_student(request):
    student = Student.objects.all()
    return render(request, template_name='list_student.html', context={'student': student})


class StudentCreate(CreateView):
    model = Student
    form_class = StudentForm
    template_name = 'add_student.html'

    def get_success_url(self):
        return reverse('course:list_student')


# edita una auditoria
class StudentEdit(UpdateView):
    model = Student
    form_class = StudentForm
    template_name = 'edit_student.html'

    def get_success_url(self):
        return reverse('course:list_student')

"""
    def get_initial(self):
        return {
            "audit": self.kwargs['pk']
        }
"""


class StudentDelete(DeleteView):
    model = Student
    form_class = StudentForm
    template_name = 'delete_student.html'

    def get_context_data(self, **kwargs):
        context_data = super(StudentDelete, self).get_context_data(**kwargs)
        pk = self.kwargs.get('pk')
        student = Student.objects.get(id=int(pk))
        context_data.update({'student': student})
        return context_data

    def get_success_url(self):
        return reverse('course:list_student')


class ListSubjects(ListView):
    template_name = 'home_teacher.html'
    model = Student

    def get(self, request, *args, **kwargs):
        name = request.GET['name']
        student_subjects = Student.objects.get(name=name)
        subjects = student_subjects.subject_student.all()
        data = serializers.serialize('json', subjects, fields=('name', 'number_credits'))
        return HttpResponse(data, content_type='application/json')
