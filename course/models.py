from __future__ import unicode_literals
from django.utils.translation import ugettext as _

from django.db import models


class Subject(models.Model):
    name = models.CharField(max_length=50)
    number_credits = models.IntegerField()

    def __unicode__(self):
        return u'{}'.format(self.name)


class Student(models.Model):
    name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    subject_student = models.ManyToManyField(Subject)

    def full_name(self):
        return u'{} {}'.format(self.name, self.last_name)

    class Meta:

        permissions = (
            ('is_teacher', _('Is Teacher')),
            ('is_student', _('Is Student')),
        )